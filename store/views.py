from django.shortcuts import render_to_response
from store.models import Twitter
from django.shortcuts import redirect
from django.http import HttpResponse
# Create your views here.

def index(request):
	return render_to_response('index.html')

def insert(request):
	twit = request.GET['twit']
	tag = request.GET['tag']
	if twit == '' and tag == '':
		direct = '/'
	else:
		ls = Twitter.objects.create(twit = twit, tag = tag)
		direct ='/select'
	return redirect(direct)

def select(request):
	if 'tag' not in request.GET:
		twit_ls = Twitter.objects.all()
		data = {'twits' : twit_ls,}

	else:
		tag = request.GET['tag']
		twit_list = Twitter.objects.filter(tag__icontains  = tag)
		data = {'twits' : twit_list,}
	return render_to_response('select.html', data)

