# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Twitter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('twit', models.CharField(max_length=15)),
                ('tag', models.URLField(max_length=15)),
            ],
        ),
        migrations.DeleteModel(
            name='Posts',
        ),
    ]
